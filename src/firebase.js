import firebase from "firebase/app";
import "firebase/auth";

// TODO: Replace the following with your app's Firebase project configuration
const firebaseConfig = {
  apiKey: "AIzaSyBfhOmo_oCDsXl0apXv7I6ToNNsCx3o4U4",
  authDomain: "devcamp-login-dff72.firebaseapp.com",
  projectId: "devcamp-login-dff72",
  storageBucket: "devcamp-login-dff72.appspot.com",
  messagingSenderId: "787494862668",
  appId: "1:787494862668:web:7419dcf768b59be9fbd9e9",
};

firebase.initializeApp(firebaseConfig);

export const auth = firebase.auth();

export const googleProvider = new firebase.auth.GoogleAuthProvider();
