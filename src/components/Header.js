import React, { useState } from "react";
import { LinkContainer } from "react-router-bootstrap";
import { Container, Nav, Navbar } from "react-bootstrap";
import SearchBox from "./SearchBox";
import avatar from "../assets/images/avatar.jpg";
import { Button, MenuItem, Menu } from "@mui/material";
import { Link, useNavigate } from "react-router-dom";
import { auth } from "../firebase.js";

const Header = () => {
  const [user, setUser] = useState(null);
  const navigate = useNavigate();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);
  const username = localStorage.name;
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };
  const handleClose = () => {
    setAnchorEl(null);
  };

  // hàm này thực hiện log out tài khoản
  const logoutGoogle = () => {
    auth
      .signOut()
      .then(() => {
        console.log("Logout successfully");
        localStorage.removeItem("name");
        localStorage.removeItem("email");
        setUser(null);
        window.location.replace("http://localhost:3000/");
      })
      .catch((error) => {
        console.log(error);
      });
  };
  return (
    <header>
      <Navbar bg="black" variant="dark" expand="lg" collapseOnSelect>
        <Container>
          <LinkContainer to="/">
            <Navbar.Brand>
              <img
                src={avatar}
                width={150}
                style={{
                  width: "30%",
                  borderRadius: "10px",
                  boxShadow: "rgba(0, 0, 0, 0.1) 0px 10px 50px",
                  backgroundColor: "#d6524e",
                  marginLeft: "-50px",
                }}
              />
            </Navbar.Brand>
          </LinkContainer>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <div>
              <SearchBox />
            </div>
            <Nav className="ml-auto">
              {/* Chuông thông báo */}
              <Button>
                <span
                  style={{ color: "white", fontSize: "17px" }}
                  className="far fa-bell "
                />
              </Button>
              {/* Đăng nhập, đăng ký */}
              <Button
                id="basic-button"
                aria-controls={open ? "basic-menu" : undefined}
                aria-haspopup="true"
                aria-expanded={open ? "true" : undefined}
                onClick={handleClick}
                style={{ color: "white" }}
              >
                {localStorage.name !== "" ? (
                  <div>
                    <span
                      style={{
                        color: "white",
                        marginRight: "10px",
                        fontSize: "20px",
                      }}
                      className="far fa-user-circle"
                    />
                    {localStorage.name}
                  </div>
                ) : (
                  <span
                    style={{
                      color: "white",
                      marginRight: "10px",
                      fontSize: "20px",
                    }}
                    className="far fa-user-circle"
                  />
                )}
              </Button>
              <Menu
                id="basic-menu"
                anchorEl={anchorEl}
                open={open}
                onClose={handleClose}
                MenuListProps={{
                  "aria-labelledby": "basic-button",
                }}
              >
                <MenuItem>
                  <Link to="/" style={{ textDecoration: "none" }}>
                    Home
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link to="/login" style={{ textDecoration: "none" }}>
                    Login
                  </Link>
                </MenuItem>
                <MenuItem>
                  <Link to="/register" style={{ textDecoration: "none" }}>
                    Create Account
                  </Link>
                </MenuItem>
                <MenuItem onClick={logoutGoogle}>Logout</MenuItem>
              </Menu>

              <Button>
                <Link to="/cart" style={{ textDecoration: "none" }}>
                  <span
                    style={{
                      color: "white",
                      fontSize: "15px",
                      marginRight: "20px",
                    }}
                    className="fas fa-shopping-cart"
                  />
                </Link>
              </Button>
              {/* Đăng nhập, đăng ký */}

              {/* <LinkContainer
                to="/login"
                style={{ fontWeight: 900, color: "white" }}
              >
                <Nav.Link>
                  <i className="fas fa-user"></i> Đăng nhập
                </Nav.Link>
              </LinkContainer>
              <LinkContainer
                to="/cart"
                style={{ fontWeight: 900, color: "white" }}
              >
                <Nav.Link>
                  <i className="fas fa-shopping-cart"></i> Giỏ hàng
                </Nav.Link>
              </LinkContainer> */}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </header>
  );
};

export default Header;
