import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Container, Form, Button, Row, Col } from "react-bootstrap";

const Filter = ({ keyword = "" }) => {
  const [minPrice, setMinPrice] = useState(0);
  const [maxPrice, setMaxPrice] = useState(999999999);
  const navigate = useNavigate();

  const filterPriceHandler = () => {
    navigate(
      keyword
        ? `/search/${keyword}/page/1/price/${minPrice}/${maxPrice}`
        : `/page/1/price/${minPrice}/${maxPrice}`
    );
  };

  return (
    <div>
      <h3 style={{ textAlign: "center", color: "blue" }}>Lọc Theo Giá</h3>
      <Container>
        <Row>
          <Col sm={12}>
            <Row>
              <Col sm={2}>
                <label
                  style={{
                    alignItems: "center",
                    paddingTop: 20,
                    marginLeft: 40,
                  }}
                >
                  Giá min
                </label>
              </Col>
              <Col sm={3}>
                <input
                  style={{ borderRadius: "10px", marginLeft: -80 }}
                  className="form-control"
                  placeholder="nhập giá min"
                  onChange={(e) => setMinPrice(e.target.value)}
                />
              </Col>
              <Col sm={2}>
                <label
                  style={{
                    alignItems: "center",
                    paddingTop: 20,
                    marginLeft: 40,
                  }}
                >
                  Giá max
                </label>
              </Col>
              <Col sm={3}>
                <input
                  style={{ borderRadius: "10px", marginLeft: -80 }}
                  className="form-control"
                  placeholder="nhập giá max"
                  onChange={(e) => setMaxPrice(e.target.value)}
                />
              </Col>
              <Col sm={2}>
                <button
                  style={{
                    backgroundColor: "green",
                    borderRadius: "10px",
                    marginTop: 10,
                    marginLeft: 40,
                    width: "100px",
                  }}
                  className="btn btn-primary"
                  onClick={filterPriceHandler}
                >
                  <span
                    style={{ width: "20px", fontWeight: 900, fontSize: "20px" }}
                    className="fas fa-filter"
                  />
                </button>
              </Col>
            </Row>
          </Col>
        </Row>
        {/* <Form>
          <Row className="mb-3">
            <Form.Group as={Col} controlId="formMinPrice">
              <Form.Label>Min Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter min price"
                onChange={(e) => setMinPrice(e.target.value)}
              />
            </Form.Group>

            <Form.Group as={Col} controlId="formMaxPrice">
              <Form.Label>Max Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter max price"
                onChange={(e) => setMaxPrice(e.target.value)}
              />
            </Form.Group>
          </Row>
          <Row className="mx-4">
            <Button
              onClick={filterPriceHandler}
              className="btn-block"
              type="button"
            >
              AP DUNG
            </Button>
          </Row>
        </Form> */}
      </Container>
    </div>
  );
};

export default Filter;
