import React from "react";
import "../App.css";
import { auth, googleProvider } from "../firebase.js";
import { useEffect, useState } from "react";
import { useNavigate, Link } from "react-router-dom";

// import { Component } from "react";
// import { Link } from "react-router-dom";
// import withFirebaseAuth from "react-with-firebase-auth";
// import firebase from "firebase/app";
// import "firebase/auth";
// import firebaseConfig from "../firebaseConfig";
// import "../App.css";
// import axios from "axios";

// const firebaseApp = firebase.initializeApp(firebaseConfig);

// class Login extends Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       inputText: "",
//     };
//   }

// hàm thực hiện việc lấy giá trị khi input thay đổi
// getChangeValue = (e) => {
//   var target = e.target;
//   var name = target.name;
//   var value = target.value;
//   this.setState({
//     [name]: value,
//   });
// };

// hàm thực hiện validate input
// checkValidateFrom = () => {
//   let vCheck = true;
//   let arrInput = ["inputText"];
//   for (let i = 0; i < arrInput.length; i++) {
//     if (!this.state[arrInput[i]]) {
//       vCheck = false;
//       alert("Bạn cần nhập đầy đủ thông tin:" + arrInput[i]);
//       break;
//     }
//   }
//   return vCheck;
// };

// loginData = (e) => {
//   e.preventDefault();
//   let vCheck = this.checkValidateFrom();
//   if (vCheck === true) {
//     axios({
//       method: "post",
//       url: "http://localhost:8001/customers-login",
//       data: {
//         inputText: this.state.inputText,
//       },
//     })
//       .then((res) => {
//         alert("đăng nhập thành công");
//         console.log(res);
//       })
//       .catch((err) => {
//         console.log(err);
//       });
//   }
// };
//   loginWithGoogle = () => [

//   ]
/*  handleLoginWithGoogle = async (e) => {
    const result = await axios.get(
      "http://localhost:3001/email-customer?email=" + e.email
    );
    if (result.data.customerModel !== null) {
      window.location.replace("http://localhost:3000/");
      console.log("dang nhap thanh congo");
    } else {
      window.location.replace("http://localhost:3000/register");
      console.log("chua co tai khoan");
    }
  }; */

// render() {
// const { user, signOut, signInWithGoogle } = this.props;
/*   if (user !== null && user !== "" && user !== undefined) {
      this.handleLoginWithGoogle(user);
    }
 */
// var { inputText } = this.state;

// return (
//   <div className="login">
//     <div className="container-login">
//       <div className="login-content">
//         <h1>Login</h1>
//         <div className="login-email">
//           <p style={{ color: "blue", textAlign: "center" }}>
//             Login width email or phone number
//           </p>
//           <input
//             style={{ ouline: "none" }}
//             type="text"
//             defaultValue={inputText}
//             onChange={this.getChangeValue}
//           />
//           <button type="button" onClick={this.loginData}>
//             Đăng nhập
//           </button>
//         </div>
//         <div className="login-google">
//           {user ? (
//             <p>Hello, {user.displayName}</p>
//           ) : (
//             <h5
//               style={{ color: "blue", textAlign: "center", paddingTop: 30 }}
//             >
//               Sign in with google.
//             </h5>
//           )}

//           {user ? (
//             <button onClick={signOut}>Sign out</button>
//           ) : (
//             <button
//               className="btn btn-success w-100"
//               onClick={signInWithGoogle}
//             >
//               Đăng nhập với google
//             </button>
//           )}

{
  /*  <label>Sign in width google.</label>
                            <button className='btn btn-success w-100' onClick={signInWithGoogle}>Đăng nhập với google</button> */
}
//             </div>
//           </div>
//           <div className="login-bottom">
//             <button
//               className="btn btn-danger w-100"
//               style={{
//                 borderRadius: "10px",
//                 boxShadow:
//                   "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
//               }}
//             >
//               <Link to="/register" style={{ color: "tomato" }}>
//                 <span style={{ color: "white", fontWeight: 900 }}>
//                   Đăng Ký Tài Khoản
//                 </span>{" "}
//               </Link>
//             </button>
//           </div>
//         </div>
//       </div>
//     );
//   }
// }

// const firebaseAppAuth = firebaseApp.auth();

// const providers = {
//   googleProvider: new firebase.auth.GoogleAuthProvider(),
// };
// export default withFirebaseAuth({
//   providers,
//   firebaseAppAuth,
// })(Login);

export const Login = () => {
  const [user, setUser] = useState(null);
  const navigate = useNavigate();

  // hàm này thực hiện login tài khoản
  const loginGoogle = () => {
    auth
      .signInWithPopup(googleProvider)
      .then((result) => {
        const name = result.user.displayName;
        const email = result.user.email;
        localStorage.setItem("name", name);
        localStorage.setItem("email", email);
        setUser(result.user);
        //navigate("/");
        window.location.replace("http://localhost:3000/");
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // hàm này thực hiện log out tài khoản
  const logoutGoogle = () => {
    auth
      .signOut()
      .then(() => {
        console.log("Logout successfully");
        localStorage.removeItem("name");
        localStorage.removeItem("email");
        setUser(null);
        window.location.replace("http://localhost:3000/");
      })
      .catch((error) => {
        console.log(error);
      });
  };

  // hàm này lưu user trên local
  useEffect(() => {
    const getValueLocalStore = () => {
      if (user !== null) {
        const name = user.displayName;
        const email = user.email;
        localStorage.setItem("name", name);
        localStorage.setItem("email", email);
      }
    };
    getValueLocalStore();
  }, []);

  useEffect(() => {
    auth.onAuthStateChanged(function (result) {
      setUser(result);
    });
  });
  return (
    <div className="login" style={{ marginBottom: 250 }}>
      <div className="container-login">
        <div className="login-content">
          <h1>Login</h1>
          <div className="login-email">
            <p style={{ color: "blue", textAlign: "center" }}>
              Login width email or phone number
            </p>

            <input
              style={{ ouline: "none", marginBottom: 10 }}
              type="text"
              placeholder="Username"
            />

            <input
              style={{ ouline: "none" }}
              type="text"
              placeholder="Pasword"
            />
            <button type="button">Sign in</button>
          </div>
          <div className="login-google">
            {user ? (
              <>
                <p style={{ textAlign: "center" }}>
                  Xin chào, {user.displayName}
                </p>
                <button onClick={logoutGoogle}>Logout</button>
              </>
            ) : (
              <button className="btn btn-success w-100" onClick={loginGoogle}>
                Login Google
              </button>
            )}
            <button className="btn btn-danger w-100" style={{ marginTop: 5 }}>
              <Link
                to="/register"
                style={{
                  textDecoration: "none",
                  color: "white",
                }}
              >
                Create a new account
              </Link>
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
