/* import React from "react";
import { useEffect, useState } from "react";
import { useHistory } from "react-router-dom";

const FilterProducts = () => {
  const [listProducts, setListProducts] = useState([]);
  const [priceMinNumber, setPriceMinNumber] = useState("");
  const [priceMaxNumber, setPricMaxNumber] = useState("");
  const [nameProduct, setNameProduct] = useState("");
   const [page, setPage] = useState({
    currentPage: 1,
    totalProductOnePage: 4,
  }); 
  const history = useHistory();

  useEffect(() => {
    async function getListProducts() {
      try {
        const result = await fetch("http://localhost:3001/products");
        const responseJson = await result.json();
        console.log(responseJson);
        const { data } = responseJson.Product;
        setListProducts(data);
      } catch (error) {
        console.log(error.message);
      }
    }
    getListProducts();
  }, []);

  const handleSearchClick = () => {
    if (priceMinNumber === "" && priceMaxNumber === "" && nameProduct === "") {
      history.push("/all");
      return;
    }
    if (priceMinNumber !== "") {
      history.push("/" + priceMinNumber);
      return;
    }
  };

  return (
    <div>
      <form>
        <div className="row">
          <div className="col">
            <input
              type="text"
              className="form-control"
              placeholder="lọc theo giá min"
              onChange={(e) => setPriceMinNumber(e.target.value)}
            />
          </div>
          <div className="col">
            <input
              type="text"
              className="form-control"
              placeholder="lọc theo giá max"
              onChange={(e) => setPricMaxNumber(e.target.value)}
            />
          </div>
          <div className="col">
            <input
              type="text"
              className="form-control"
              placeholder="lọc theo tên sản phẩm"
              onChange={(e) => setNameProduct(e.target.value)}
            />
          </div>
          <div className="col">
            <button onClick={handleSearchClick}>Search</button>
          </div>
        </div>
      </form>
    </div>
  );
};
export default FilterProducts;
 */
