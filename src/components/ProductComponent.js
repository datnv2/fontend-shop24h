import React from "react";
import { Card } from "react-bootstrap";
import { Link } from "react-router-dom";

const ProductComponent = ({ product }) => {
  return (
    <div>
      <Card
        className="my-3 p-3 rounded"
        style={{
          boxShadow:
            "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
          borderRadius: "10px",
        }}
      >
        <Link to={`/products/${product._id}`}>
          <Card.Img src={product.imageUrl} variant="top" />
        </Link>

        <Card.Body>
          {/* <Link to={`/products/${product._id}`} a> */}
          <Card.Title as="div">
            <strong>{product.name}</strong>
          </Card.Title>
          {/* </Link> */}

          <Card.Text as="h6">
            <h5
              style={{
                /* textDecoration: "line-through", */ color: "red",
                fontWeight: 800,
              }}
            >
              buyPrice:{product.buyPrice}
            </h5>
          </Card.Text>

          <Card.Text style={{ color: "red" }} as="h6">
            <h6
              style={{ color: "red", fontWeight: "bolder", fontSize: "12px" }}
            >
              promotionPrice:{product.promotionPrice}
            </h6>
            {/*  promotionPrice: {product.promotionPrice} */}
          </Card.Text>
        </Card.Body>
      </Card>
    </div>
  );
};

export default ProductComponent;
