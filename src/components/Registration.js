import React, { useState } from "react";
import { Link } from "react-router-dom";
import axios from "axios";

import { Modal, Button } from "react-bootstrap";

const Registration = () => {
  const [show, setShow] = useState(false);

  // hàm thực hiện set lại giá trị cho modal mở lên
  const handlerModalShow = () => {
    setShow(true);
  };

  // hàm thực hiện set lại giá trị cho modal đóng lại
  const handlerModalHide = () => {
    setShow(false);
  };
  // set giá trị cho fullName
  const [fullName, setFullName] = useState(
    () => window.localStorage.getItem("hooksFullName") || ""
  );
  const [fullNameErr, setFullNameCheck] = useState("");

  //set giá trị cho phoneNumber
  const [phoneNumber, setPhoneNumber] = useState(
    () => window.localStorage.getItem("hooksPhoneNumber") || ""
  );
  const [phoneNumberErr, setPhoneNumberCheck] = useState("");

  // set giá trị cho email
  const [email, setEmail] = useState(
    () => window.localStorage.getItem("hooksEmail") || ""
  );
  const [emailErr, setEmailCheck] = useState("");

  // set giá trị cho password
  const [password, setPassword] = useState(
    () => window.localStorage.getItem("hooksPassword") || ""
  );
  const [passwordErr, setPasswordCheck] = useState("");

  // set giá trị cho adress
  const [address, setAddress] = useState(
    () => window.localStorage.getItem("hooksAddress") || ""
  );
  const [addressErr, setAdressCheck] = useState("");

  // set giá trị cho city
  const [city, setCity] = useState(
    () => window.localStorage.getItem("hooksCity") || ""
  );
  const [cityErr, setCityCheck] = useState("");

  // set giá trị cho country
  const [country, setCountry] = useState(
    () => window.localStorage.getItem("hooksCountry") || ""
  );
  const [countryErr, setCountryCheck] = useState("");

  // check fullName
  const handleFullNameChange = (e) => {
    setFullName(e.target.value);

    if (!e.target.value) {
      setFullNameCheck("fullName is not Empty");
    }
  };

  // check phoneNumber
  const handlePhoneNumberChange = (e) => {
    setPhoneNumber(e.target.value);

    if (!e.target.value) {
      setPhoneNumberCheck("phoneNumber is not Empty");
    }
  };

  // check email
  const handleEmailChange = (e) => {
    setEmail(e.target.value);

    if (!e.target.value) {
      setEmailCheck("Email is not Empty");
    }
  };

  // check password
  const handlePasswordChange = (e) => {
    setPassword(e.target.value);

    if (!e.target.value) {
      setPasswordCheck("Password is not Empty");
    }
  };
  // check adress
  const handleAdressChange = (e) => {
    setAddress(e.target.value);

    if (!e.target.value) {
      setAdressCheck("adress is not Empty");
    }
  };

  // check city
  const handleCityChange = (e) => {
    setCity(e.target.value);

    if (!e.target.value) {
      setCityCheck("city is not Empty");
    }
  };

  // check Country
  const handleCountryChange = (e) => {
    setCountry(e.target.value);

    if (!e.target.value) {
      setCountryCheck("country is not Empty");
    }
  };

  // thực hiện gọi api tạo user
  function createUser(e) {
    e.preventDefault();
    const userData = {
      fullName,
      phoneNumber,
      email,
      password,
      address,
      city,
      country,
    };
    axios
      .post(`http://localhost:8001/customers`, userData)
      .then((response) => {
        console.log(response);
        handlerModalShow();
        //alert("Đã tạo tài khoản thành công");
      })
      .catch((error) => {
        console.log(error);
      });
  }

  return (
    <div style={{ marginBottom: 150 }}>
      <div>
        <h2 style={{ textAlign: "center", color: "blue", marginTop: "10px" }}>
          ĐĂNG KÝ
        </h2>
        <form className="form-register" onSubmit={createUser}>
          <div className="register">
            <label>FullName</label>
            <input
              type="text"
              placeholder="fullName"
              value={fullName}
              onChange={handleFullNameChange}
              required
            />
          </div>
          <span style={{ color: "red" }}>{fullNameErr}</span>
          <div className="register">
            <label> PhoneNumber</label>
            <input
              type="text"
              placeholder="PhoneNumber"
              value={phoneNumber}
              onChange={handlePhoneNumberChange}
              required
            />
          </div>
          <span style={{ color: "red" }}>{phoneNumberErr}</span>
          <div className="register">
            <label> Email</label>
            <input
              type="text"
              placeholder="email"
              value={email}
              onChange={handleEmailChange}
              required
            />
          </div>
          <span style={{ color: "red" }}>{emailErr}</span>
          <div className="register">
            <label> Password</label>
            <input
              type="text"
              placeholder="password"
              value={password}
              onChange={handlePasswordChange}
              required
            />
          </div>
          <span style={{ color: "red" }}>{passwordErr}</span>
          <div className="register">
            <label> Address</label>
            <input
              type="text"
              placeholder="address"
              value={address}
              onChange={handleAdressChange}
              required
            />
          </div>
          <span style={{ color: "red" }}>{addressErr}</span>
          <div className="register">
            <label> City</label>
            <input
              type="text"
              placeholder="city"
              value={city}
              onChange={handleCityChange}
              required
            />
          </div>
          <span style={{ color: "red" }}>{cityErr}</span>
          <div className="register">
            <label> Country</label>
            <input
              type="text"
              placeholder="country"
              value={country}
              onChange={handleCountryChange}
              required
            />
          </div>
          <span style={{ color: "red" }}>{countryErr}</span>
          <button type="submit">Đăng Ký</button>
          <button>
            <Link to="/Login" style={{ textDecoration: "none" }}>
              <span
                style={{
                  color: "white",
                  textAlign: "center",
                  marginLeft: "-4px",
                }}
              >
                {" "}
                Quay Lại
              </span>
            </Link>
          </button>
        </form>
      </div>
      <Modal show={show} onHide={handlerModalHide}>
        <Modal.Header closeButton>
          <Modal.Title style={{ color: "red" }}>Thông báo</Modal.Title>
        </Modal.Header>
        <Modal.Body style={{ color: "blue" }}>
          Đã tạo tài khoản thành công!
        </Modal.Body>
        <Modal.Footer>
          <Button
            variant="success"
            onClick={handlerModalHide}
            style={{ borderRadius: "10px" }}
          >
            Đóng
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default Registration;
