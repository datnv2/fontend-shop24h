import React, { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Form, Button, Row, Col } from "react-bootstrap";

export const SearchBox = () => {
  const [keyword, setKeyword] = useState("");
  const navigate = useNavigate();

  const submitHandler = (e) => {
    e.preventDefault();
    if (keyword.trim()) {
      navigate(`/search/${keyword}`);
    } else {
      navigate("/");
    }
  };
  return (
    <div style={{ marginRight: "200px" }}>
      <Row>
        <Col sm={12}>
          <Row>
            <Col sm={8}>
              <input
                className="form-control"
                onChange={(e) => setKeyword(e.target.value)}
                placeholder="Search Products..."
                style={{
                  width: "500px",
                  marginTop: "5%",
                  borderRadius: "10px",
                  backgroundColor: "#ffffff",
                  marginRight: "10px",
                  border: "none",
                }}
                type="text"
                name="q"
              />
            </Col>
            <Col sm={4}>
              <button
                className="btn btn-primary"
                style={{
                  borderRadius: "10px",
                  //marginTop: 10,
                  marginLeft: 95,
                  backgroundColor: "#fa9f2c",
                  marginTop: "10%",
                  width: "55px",
                  height: "49px",
                  border: "none",
                }}
                onClick={submitHandler}
              >
                <span className="fa-solid fa-magnifying-glass" />
              </button>
            </Col>
          </Row>
        </Col>
      </Row>
      {/* <Form>
        <Form.Control
          type="text"
          name="q"
          placeholder="Search Products..."
          onChange={(e) => setKeyword(e.target.value)}
          className="mr-sm-2 ml-sm-5"
          style={{ borderRadius: "10px" }}
        ></Form.Control>
        <Button
          style={{ marginLeft: "-100px" }}
          type="submit"
          variant="outline-success"
          className="p-2"
          onClick={submitHandler}
        >
          Search
        </Button>
      </Form> */}
    </div>
  );
};
export default SearchBox;
