import React from "react";
import products from "../products.js";
import { Carousel, Container } from "react-bootstrap";
//import "../App.css";

const ProductCarousel = () => {
  return (
    <Container style={{ marginTop: 30 }}>
      <Carousel>
        <Carousel.Item>
          <img
            className="d-block "
            src={products[3].imageUrl}
            alt="First slide"
          />
          <Carousel.Caption>
            {/*    <h3> {products[8].name}</h3> */}
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block"
            src={products[1].imageUrl}
            alt="Second slide"
          />

          <Carousel.Caption></Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block "
            src={products[2].imageUrl}
            alt="Third slide"
          />

          <Carousel.Caption></Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block "
            src={products[11].imageUrl}
            alt="Fourd slide"
          />

          <Carousel.Caption></Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </Container>
  );
};

export default ProductCarousel;
