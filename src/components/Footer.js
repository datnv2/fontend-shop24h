import React from "react";
import { Container, Col, Row } from "react-bootstrap";
import avatar from "../assets/images/avatar.jpg";
import { Link } from "react-router-dom";
import "../App.css";

const Footer = () => {
  return (
    <div className="footer ">
      <footer
        className="bg-black w-100 fixed-bottom "
        style={{ paddingTop: 20 }}
      >
        <div>
          <Row>
            <Col xs={3} md={3} lg={3}>
              <div style={{ marginLeft: 150 }}>
                <h5 className="text-white">PRODUCTS</h5>
                <ul className="list-unstyled ">
                  <li className="text-black">
                    <a className="footer_item">Help Center </a>
                  </li>
                  <li className="text-black">
                    <a className="footer_item">Contact Us </a>
                  </li>
                  <li className="text-black">
                    <a className="footer_item">Product Help</a>
                  </li>
                  <li className="text-black">
                    <a className="footer_item">Warranty</a>
                  </li>
                  <li className="text-black">
                    <a className="footer_item">Order Status</a>
                  </li>
                </ul>
              </div>
            </Col>

            <Col xs={3} md={3} lg={3}>
              <div style={{ marginLeft: 150 }}>
                <h5 className="text-white">SERVICES</h5>

                <ul className="list-unstyled ">
                  <li className="text-black">
                    <a className="footer_item">Help Center </a>
                  </li>
                  <li className="text-black">
                    <a className="footer_item">Contact Us </a>
                  </li>
                  <li className="text-black">
                    <a className="footer_item">Product Help</a>
                  </li>
                  <li className="text-black">
                    <a className="footer_item">Warranty</a>
                  </li>
                  <li className="text-black">
                    <a className="footer_item">Order Status</a>
                  </li>
                </ul>
              </div>
            </Col>

            <Col xs={3} md={3} lg={3}>
              <div style={{ marginLeft: 150 }}>
                <h5 className="text-white">SUPPORT</h5>

                <ul className="list-unstyled ">
                  <li className="text-black">
                    <a className="footer_item">Help Center </a>
                  </li>
                  <li className="text-black">
                    <a className="footer_item">Contact Us </a>
                  </li>
                  <li className="text-black">
                    <a className="footer_item">Product Help</a>
                  </li>
                  <li className="text-black">
                    <a className="footer_item">Warranty</a>
                  </li>
                  <li className="text-black">
                    <a className="footer_item">Order Status</a>
                  </li>
                </ul>
              </div>
            </Col>

            <Col xs={3} md={3} lg={3}>
              <div style={{ marginLeft: 150 }}>
                <Link to="/">
                  <a className="navbar-brand">
                    <img
                      src={avatar}
                      alt=""
                      width={100}
                      style={{
                        width: "40%",
                        borderRadius: "10px",
                        boxShadow: "rgba(0, 0, 0, 0.1) 0px 10px 50px",
                        backgroundColor: "#d6524e",
                      }}
                    />
                  </a>
                </Link>

                <ul className="list-unstyled " style={{ paddingTop: 10 }}>
                  <li className="text-black">
                    <i
                      style={{ margin: 5, color: "white", fontSize: "20px" }}
                      className="fab fa-facebook"
                    ></i>
                    <i
                      style={{ margin: 5, color: "white", fontSize: "20px" }}
                      className="fab fa-instagram-square"
                    ></i>
                    <i
                      style={{ margin: 5, color: "white", fontSize: "20px" }}
                      className="fab fa-youtube"
                    ></i>
                    <i
                      style={{ margin: 5, color: "white", fontSize: "20px" }}
                      className="fab fa-twitter"
                    ></i>
                  </li>
                </ul>
              </div>
            </Col>
          </Row>
        </div>
      </footer>
    </div>

    /*  <footer>
      <Container>
        <Row>
          <Col className="text-center py-3">Copyright &copy; Shop 24h</Col>
        </Row>
      </Container>
    </footer> */
  );
};

export default Footer;
