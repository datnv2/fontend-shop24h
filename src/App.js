import React from "react";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import { Container } from "react-bootstrap";
import Header from "./components/Header";
import Footer from "./components/Footer";
import HomeScreen from "./screens/HomeScreen";
import ProductScreen from "./screens/ProductScreen";
import CartScreen from "./screens/CartScreen";
import ShippingScreen from "./screens/ShippingScreen";
import Login from "./components/Login";
import Registration from "./components/Registration";

function App() {
  return (
    <React.StrictMode>
      <BrowserRouter>
        <Header />
        <main className="py-3">
          <Container>
            <Routes>
              <Route path="/" element={<HomeScreen />} />
              <Route path="/search/:keyword" element={<HomeScreen />} />
              <Route path="/page/:pageNumber" element={<HomeScreen />} />
              <Route
                path="/price/:minPrice/:maxPrice"
                element={<HomeScreen />}
              />
              <Route
                path="/search/:keyword/page/:pageNumber"
                element={<HomeScreen />}
              />
              <Route
                path="/search/:keyword/price/:minPrice/:maxPrice"
                element={<HomeScreen />}
              />
              <Route
                path="/page/:pageNumber/price/:minPrice/:maxPrice"
                element={<HomeScreen />}
              />
              <Route
                path="/search/:keyword/page/:pageNumber/price/:minPrice/:maxPrice"
                element={<HomeScreen />}
              />
              <Route path="/products/:id" element={<ProductScreen />} />
              <Route path="/cart" element={<CartScreen />} />
              <Route path="/cart/:id/*" element={<CartScreen />} />
              <Route path="/shipping" element={<ShippingScreen />} />
              <Route path="/Login" element={<Login />} />
              <Route path="/register" element={<Registration />} />
              {/* <Route path="/register" element={<Registration />} /> */}
            </Routes>
          </Container>
        </main>
        <Footer />
      </BrowserRouter>
    </React.StrictMode>
  );
}

export default App;
