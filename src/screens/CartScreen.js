import React, { useEffect } from "react";
import { Link, useParams, useLocation, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { Row, Col, ListGroup, Image, Button } from "react-bootstrap";
import Message from "../components/Message";
import { addToCart, removeFromCart } from "../actions/cartActions";

const CartScreen = () => {
  const params = useParams();
  const location = useLocation();
  const navigate = useNavigate();

  const productId = params.id;
  const quantity = location.search ? Number(location.search.split("=")[1]) : 1;

  const dispatch = useDispatch();

  const cart = useSelector((state) => state.cart);
  const { cartItems } = cart;

  console.log(cartItems);

  useEffect(() => {
    if (productId) {
      dispatch(addToCart(productId, quantity));
    }
  }, [dispatch, productId, quantity]);

  const removeFromCartHandler = (id) => {
    dispatch(removeFromCart(id));
    navigate("/cart");
  };

  const checkoutHandler = () => {
    navigate("/shipping");
  };

  // kiểm tra xem có user hay không
  useEffect(() => {
    if (localStorage.name === undefined || localStorage.name === "") {
      navigate("/login");
    }
  });

  return (
    <div style={{ marginBottom: 100 }}>
      <Row>
        <Col md={8}>
          <h1>Shopping Cart</h1>
          {cartItems.length === 0 ? (
            <Message>
              Your cart is empty <Link to="/">Go Back</Link>
            </Message>
          ) : (
            <ListGroup variant="flush">
              {cartItems.map((item) => (
                <ListGroup.Item key={item.product}>
                  <Row>
                    <Col md={2}>
                      <Image
                        src={item.image}
                        alt={item.name}
                        fluid
                        rounded
                      ></Image>
                    </Col>
                    <Col md={3}>
                      <Link to={`/products/${item.product}`}>{item.name}</Link>
                    </Col>
                    <Col md={2}>{item.price} VND</Col>
                    <Col md={2}>
                      <input
                        style={{ borderRadius: "10px" }}
                        type="number"
                        className="form-control"
                        value={item.quantity}
                        onChange={(e) =>
                          dispatch(
                            addToCart(item.product, Number(e.target.value))
                          )
                        }
                      />
                    </Col>
                    <Col md={2}>
                      <Button
                        type="button"
                        variant="light"
                        onClick={() => removeFromCartHandler(item.product)}
                      >
                        <i className="fas fa-trash"></i>
                      </Button>
                    </Col>
                  </Row>
                </ListGroup.Item>
              ))}
            </ListGroup>
          )}
        </Col>
        <Col md={4}>
          <ListGroup variant="flush">
            <ListGroup.Item>
              <h2>
                Subtotal{" "}
                {cartItems.reduce((acc, item) => acc + item.quantity, 0)} items
              </h2>
              {cartItems.reduce(
                (acc, item) => acc + item.quantity * item.price,
                0
              )}{" "}
              VND
            </ListGroup.Item>
            <ListGroup.Item>
              <Row className="mx-1">
                <Button
                  onClick={checkoutHandler}
                  type="button"
                  className="btn-block"
                  disabled={cartItems.length === 0}
                  style={{
                    backgroundColor: "red",
                    borderRadius: "10px",
                  }}
                >
                  PROCEED TO CHECKOUT
                </Button>
              </Row>
            </ListGroup.Item>
          </ListGroup>
        </Col>
      </Row>
    </div>
  );
};

export default CartScreen;

{
  /* <div>
"proxy": "http://127.0.0.1:3001",
</div> */
}
