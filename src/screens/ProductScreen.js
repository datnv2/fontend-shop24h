import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams, useNavigate } from "react-router-dom";
import { Row, Col, Image, ListGroup, Card, Button } from "react-bootstrap";
import { listProductDetails } from "../actions/productActions";
import Loader from "../components/Loader";

const ProductScreen = () => {
  const [quantity, setQuantity] = useState(1);

  const params = useParams();
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const { loading, product } = useSelector((state) => state.productDetails);

  useEffect(() => {
    dispatch(listProductDetails(params.id));
  }, [dispatch, params.id]);

  const addToCartHandler = () => {
    navigate(`/cart/${params.id}?quantity=${quantity}`);
  };

  const changeQuantity = (e) => {
    setQuantity(e.target.value);
  };

  return (
    <div style={{ marginBottom: 200 }}>
      <Link
        className="btn =btn-primary my-3"
        to="/"
        style={{
          backgroundColor: "orange",
          borderRadius: "10px",
          color: "white",
        }}
      >
        Go Back
      </Link>

      {loading ? (
        <Loader />
      ) : (
        <Row>
          <Col md={6}>
            <Image
              // style={{ borderRadius: "10px" }}
              // style={{
              //   width: "15rem",
              //   height: "15rem",
              //   objectFit: "cover",
              //   marginTop: "1rem",
              // }}
              src={product.imageUrl}
              alt={product.name}
              fluid
            ></Image>
          </Col>

          <Col md={3}>
            <ListGroup variant="flush">
              <ListGroup.Item>
                <h3
                // style={{
                //   whiteSpace: "nowrap",
                //   overflow: "hidden",
                //   textOverflow: "ellipsis",
                //   width: "250px",
                // }}
                >
                  {product.name}
                </h3>
              </ListGroup.Item>
              <ListGroup.Item>Buy Price: {product.buyPrice}</ListGroup.Item>
              <ListGroup.Item>
                Promotion: {product.promotionPrice}
              </ListGroup.Item>
              <ListGroup.Item>
                Description: {product.description}
              </ListGroup.Item>
            </ListGroup>
          </Col>

          <Col md={3} style={{ paddingBottom: "100px" }}>
            <Card
              style={{
                // width: "18rem",
                marginTop: "20px",
                boxShadow:
                  "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
                borderRadius: "10px",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <ListGroup variant="flush">
                <ListGroup.Item>
                  <Row>
                    <Col>Price: </Col>
                    <Col>
                      <strong>${product.promotionPrice}</strong>
                    </Col>
                  </Row>
                </ListGroup.Item>
                <ListGroup.Item>
                  <Row>
                    <Col>Quantity: </Col>
                    <Col>
                      <input
                        style={{ borderRadius: "10px" }}
                        className="form-control"
                        type="number"
                        value={quantity}
                        onChange={changeQuantity}
                      />
                    </Col>
                  </Row>
                </ListGroup.Item>
                <ListGroup.Item>
                  <Row className="mx-1">
                    <Button
                      onClick={addToCartHandler}
                      className="btn-block w-100"
                      type="button"
                      style={{
                        backgroundColor: "green",
                        borderRadius: "10px",
                        paddingTop: 10,
                        width: "100%",
                        textAlign: "center",
                      }}
                    >
                      Add to cart
                    </Button>
                  </Row>
                </ListGroup.Item>
              </ListGroup>
            </Card>
          </Col>
        </Row>
      )}
    </div>
  );
};

export default ProductScreen;
