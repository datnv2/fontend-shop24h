import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { Row, Col } from "react-bootstrap";
import ProductComponent from "../components/ProductComponent";
import { listProducts } from "../actions/productActions";
import Loader from "../components/Loader";
import Pagination from "../components/Pagination";
import ProductCarousel from "../components/ProductCarousel";
import "../App.css";
import Filter from "../components/Filter";

const HomeScreen = () => {
  const dispatch = useDispatch();
  const keyword = useParams().keyword;
  const pageNumber = useParams().pageNumber || 1;
  const minPrice = useParams().minPrice;
  const maxPrice = useParams().maxPrice;

  const productList = useSelector((state) => state.productList);

  const { success, Product, page, pages } = productList;

  useEffect(() => {
    dispatch(listProducts(keyword, pageNumber, minPrice, maxPrice));
  }, [dispatch, keyword, pageNumber, minPrice, maxPrice]);
  return (
    <>
      <div>
        <ProductCarousel />
      </div>

      {/* <h1
        style={{
          color: "blue",
          textAlign: "center",
          fontWeight: 900,
          marginTop: "-12px",
        }}
      >
        DANH SÁCH SẢN PHẨM
      </h1> */}

      <Filter keyword={keyword && keyword} />

      {success ? (
        <>
          <Row>
            {Product.map((product) => (
              <Col sm={12} md={6} lg={4} xl={3}>
                <ProductComponent product={product} />
              </Col>
            ))}
          </Row>
          <Pagination pages={pages} page={page} keyword={keyword && keyword} />
        </>
      ) : (
        <Loader />
      )}
    </>
  );
};

export default HomeScreen;
